import React from 'react';
import { AsyncStorage } from 'react-native';
import sysUri from '../config/SysUri';

const const_users = new Map()

export default class CacheUsers{
    static constructor(){
        profile = null
    }

    static async getProfile(){
        if(this.profile == null){
            await AsyncStorage.getItem('Profile').then((res)=> {
                if (res !== null){
                    this.profile =  JSON.parse(res)
                    console.log(this.profile)
                    return this.profile
                }
            }).catch((error) => {
                console.error(error);
            });
        }else{
            return this.profile
        }
    }
   
    static async getUsers(){
        if(const_users.size == 0){
            await fetch(sysUri.allUsers).then(res=>{
                return res.json()
            }).then(json=>{
                // console.log(json)
                json.map((item)=>{
                    const_users.set(item.IDUSER,item)
                })
                
            }).then(()=>{
                
                return const_users
            }).catch((error) => {
                console.error(error)
            })

        }else{
            return const_users
        }

    }
    static async getUser(key){
        return const_users.get(key)
    }
}