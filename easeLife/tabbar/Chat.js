import React from 'react';
import { AppRegistry, StyleSheet, Text, View,ScrollView, ActivityIndicator,AsyncStorage,Alert } from 'react-native';
import { Button, Tabs, Tab, Icon, Header, List, ListItem, Avatar  } from 'react-native-elements';
import PubSub from 'pubsub-js';

import sysColor from '../config/SysColor';
import sysUri from '../config/SysUri';
import style from '../static/style';

import CacheUser from '../cache/CacheUser';
import instanceMessage from '../cache/instanceMessage';
import ChatWith from '../chat/ChatWith';


export default class Chat extends React.Component {
  constructor() {
    super(); 
    this.state = {
      profile: {},
      friends: [],
      newMessage:  instanceMessage,
      notifyUser: [],
    }
    this.ws = new WebSocket(sysUri.wss)
    this.resetNotify = this.resetNotify.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    // this.setFriend = this.setFriend.bind(this)
  }
  static navigationOptions = ({navigation, screenProps}) => ({
     tabBarIcon: ({ tintColor }) => 
        <Icon  color={tintColor} name='forum' size={25} />,
     headerRight:  <Icon
        name='add'
        color={sysColor.white}
        onPress={() =>
          navigation.navigate('AddNewFriend')
        }
        containerStyle = {{margin: 5, marginRight: 10}}
      />, 
  })

  resetNotify(key){
    var tempNewMessage = this.state.newMessage
    tempNewMessage.delete(key)
    this.setState({
      newMessage: tempNewMessage
    })
  }

  sendMessage(message){
    this.ws.onopen = function () {
      this.ws.send(message);
    }
    
  }

  async componentWillMount(){
    this.signonCheck();
  }

  async signonCheck(){
    console.log("Going in signonCheck ...")
       await AsyncStorage.getItem('Profile').then((profile)=> {
        if (profile !== null){
            this.setState({
              isChecking: false,
              profile: JSON.parse(profile),
            })
          if (this.state.profile){
            var idUser = this.state.profile.IDUSER
            this.loadFriend(idUser)
          }
        }else{
          this.setState({
            profile: null,
            friends: []
          })
        }
      }).catch((error) => {
          this.setState({
            isChecking: false,
            error:true,
            errorInfo:"Local profile loading error ...",
          })
          console.error(error);
        });
  }

  componentDidMount(){
    
    var signout = PubSub.subscribe( 'Signout', (msg, data) => {
      this.signonCheck()
      
    } );

    var logon = PubSub.subscribe( 'Logon', (msg, data) => {
      this.signonCheck()
    } );

    if(this.state.profile){
      this.ws.onmessage = (e) => {
      // a message was received
      // console.log(e)
      
      console.log(e.data)
      var data = JSON.parse(e.data)
      data.map(ele => {
        console.log(ele)
        
        var message = ele.message
        CacheUser.getUser(ele.from).then((fromUser)=>{
          message._id = Date.now() 
          console.log(fromUser)
          console.log(fromUser.IMAGE)
          message.user = {
            _id: fromUser.IDUSER,
            name: fromUser.NAME,
            avatar: sysUri.loadImg+'/'+fromUser.IMAGE
          }
          PubSub.publish( 'NewMessages', ele );
          
          var friendHasMessage = this.state.newMessage.get(ele.from)
          //Push new message to a Map
          tempNewMessage = this.state.newMessage
          if(friendHasMessage){
            friendHasMessage.push(ele)
            tempNewMessage.set(ele.from, friendHasMessage)
          }else{
            tempNewMessage.set(ele.from, [ele])
          }
          this.setState({
            newMessage: tempNewMessage
          })
          console.log(this.state.newMessage)   
        }).catch(error=>{
          console.log(error)
        })
        
      })
    };
   
    this.ws.onopen = () => {
      // connection opened
      this.ws.send(JSON.stringify({idUser: this.state.profile.IDUSER, type:'conn'})); // send a message
    };

    this.ws.onerror = (e) => {
      // an error occurred
      console.log(e.message);
    };
    }
    

    this.ws.onclose = (e) => {
      // connection closed
      console.log(e);
    };
  }

  async loadFriend(idUser){
    await CacheUser.getUsers()
    await fetch(sysUri.myFriend+'/'+idUser+'/'+idUser ).then((res) =>{
      return res.json()
    }).then((json)=>{
      json.map(item=>{
        var oldFriends = this.state.friends
        if(item.IDUSER == this.state.profile.IDUSER){
          CacheUser.getUser(item.IDFRIEND).then((friend) =>{
            oldFriends.push(friend)
            this.setState({
              friends: oldFriends
            })
          })
        }else{
          CacheUser.getUser(item.IDUSER).then((friend) =>{
            oldFriends.push(friend)
            this.setState({
              friends: oldFriends
            })
          })
        }
      })
    }).catch((error)=>{
      console.log(error)
    })
  }

 
  renderLoadingView(){
    return (
      <ScrollView>
       <View style={style.cardDisplay}>
          <ActivityIndicator
              animating={true}
              style={{height: 80}}
              color={sysColor.green}
              size="large"
          />
      </View>
      </ScrollView>
    )
  }

  render() {
   
    if (this.state.friends.length == 0){
      this.renderLoadingView()
    }

    if(this.state.profile){
      return(
        <ScrollView>
          <Text>{this.state.notifyUser}</Text> 
        <List>
        {this.state.friends.map((friend,i) => {
          {/* console.log(this.state.newMessage)
          console.log(friend)

          console.log(this.state.newMessage.get(friend.IDUSER)) */}
          if(!this.state.newMessage.get(friend.IDUSER)){
            return (
              <ListItem
                onPress = {()  => {
                  this.setState({
                    currentTalkingFriend:friend.IDUSER
                  })
                  this.props.navigation.navigate('ChatWith', 
                    { profile:this.state.profile, 
                      friend: friend,
                      friendName:friend.NAME, 
                      resetNotify: this.resetNotify,
                      sendMessage: this.sendMessage,
                      currentTalkingFriend: friend.IDUSER
                      })
                      }}
                avatar={{uri:sysUri.loadImg+'/'+friend.IMAGE}}
                key={i}
                title={friend.NAME}
                hideChevron={true} 
                subtitle={
                  <View style={style.subtitleView}>
                    <Text style={style.ratingText}>{friend.DESCRIPTION}</Text>
                  </View>}
              />
            )
          }else{
            return (
              <ListItem
                onPress = {() => {
                  this.setState({
                    currentTalkingFriend:friend.IDUSER
                  })
                  this.props.navigation.navigate('ChatWith', 
                    { profile:this.state.profile, 
                      friend: friend,
                      friendName:friend.NAME, 
                      resetNotify: this.resetNotify,
                      sendMessage: this.sendMessage,
                      currentTalkingFriend: friend.IDUSER,
                      })}}
                avatar={{uri:sysUri.loadImg+'/'+friend.IMAGE}}
                key={i}
                title={friend.NAME}
                hideChevron={true} 
                subtitle={
                  <View style={style.subtitleView}>
                    <Text style={style.ratingText}>{friend.DESCRIPTION}</Text>
                  </View>}
                badge={{ value: this.state.newMessage.get(friend.IDUSER).length, textStyle: { color: sysColor.white }, containerStyle: { backgroundColor:sysColor.pink, marginTop:5} }}
              />
            )
          }
          
          })}
          
        </List>
        </ScrollView>
      )
    }else{
      return (<View><Text style={{backgroundColor:sysColor.white, padding:10, color:sysColor.gray, margin:5, marginTop:20}}>Please Logon/Signup first to view your message...</Text></View>)
    }

    
  }

}
