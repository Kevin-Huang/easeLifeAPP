import React from 'react';
import { StyleSheet, ScrollView, Text, View } from 'react-native';
import { Button, Tabs, Tab, Icon, Header  } from 'react-native-elements';

import sysColor from '../config/SysColor';

import shopApi from '../api/ShopApi';

import DisplayShop from '../shopList/DisplayShop';

export default class ShopList extends React.Component {
  constructor() {
    super();
    this.state = {
      shops: [],
      isCache: 'false'
    }
  }
  
  static navigationOptions = {
    title: 'Shop List',
    tabBarLabel: 'Shops',
    headerStyle: {
      backgroundColor: sysColor.pink,
    },
    // Note: By default the icon is only shown on iOS. Search the showIcon option below.
    tabBarIcon: ({ tintColor }) => 
        <Icon  color={tintColor} name='store' size={25} />
  };
  
  render() {
    displayShop = [];
    console.log('before for');
    // for( let item in cacheShop.values()){
    //   console.log('for for for');
    //    displayShop.push(<DisplayShop shop={item} key={item.idSHOP} navigation={this.props.navigation} />)
    // }
    return (
        <ScrollView >  
           {console.log(this.state.shops)}
           {this.state.shops.map((item,index) => {
             return <DisplayShop shop={item} key={index} navigation={this.props.navigation} />
           })}
        </ScrollView>
    );
  }

  componentDidMount(){
    if(this.state.isCache == 'false'){
      console.log('load again load again');
      shopApi.getShops().then((res) => {
          this.setState({
            shops: res,
            isCache: 'true'
          })
        })
      }
      
  }

}