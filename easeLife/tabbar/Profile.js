import React from 'react';
import { StyleSheet, ScrollView, Text, TextInput, View, AsyncStorage, ActivityIndicator } from 'react-native';
import {  Header, Tile, Icon, Avatar,List,Button,switchButton, ListItem,Divider, Card, Grid, Col, Row } from 'react-native-elements';
import {
  TabNavigator,
  StackNavigator
} from 'react-navigation';
import PubSub from 'pubsub-js';
import EaseLifeBase from '../prototype/EaseLifeBase';

import sysColor from '../config/SysColor';
import sysUri from '../config/SysUri';
import style from '../static/style';
import FetchJson from '../api/FetchJson';

export default class Profile extends EaseLifeBase {
  constructor() {
    super();
    this.state = {
      profile: null,
      error: false,
      errorInfo: "",
      isChecking: false,
      phone: "",
    };
  }

  static navigationOptions = {
     tabBarIcon: ({ tintColor }) => 
        <Icon  color={tintColor} name='person' size={25} />
  };

  componentDidMount(){
    this.signonCheck();
  }

  async signout(){
    await AsyncStorage.removeItem('Profile').then( async () => {
      this.setState({
        profile: null
      });
      PubSub.publish( 'Signout', 'Signout' );
      
    }  
  ).catch((err) => {
    console.log(err);
    throw err;
  });
  }

  handleChange(text) {
    this.setState({phone: text});
  }

  async getCode(){
    //ToDo: Test only, 
    //
    var phone = this.state.phone;
    this.setState({
      isChecking: true,
    });
    fetch(sysUri.profileByPhone+'/'+phone)
      .then((res) => {
        if(res.headers.get("content-length") > 0){
          res.json().then( async (json) => {
             this.setState({
              isChecking: false,
              error: false,
              errorInfo: "",
              profile: json,
            });
            console.log(json+Math.random());
            await AsyncStorage.setItem('Profile', JSON.stringify(json)).then((err,ee)=>{
              console.log(err)
            });
            PubSub.publish( 'Logon', 'Logon' );
       })}else{
          this.setState({
              isChecking: false,
              error: true,
              profile: null,
              errorInfo: "Your phone number is not be regiested yet, \nPlase input the CODE you received by this phone number to active an EaseLife account identified by: " + phone,
            });
       }
        
    }).catch((error)=>{
          this.setState({
            isChecking: false,
            error: true,
            errorInfo: "Internal Error...",
        });
        throw error;
      });

  }

  render() {
    if (this.state.isChecking){
      return this.renderLoadingView();
    }
    if (this.state.error) {
      return (
        <View>
          {this.renderErrorView(this.state.errorInfo)}
          {this.renderSignonSignup()}
        </View>
        );
    }

    if(this.state.profile==null){
      return this.renderSignonSignup()
    }else{
      
      return (
      <ScrollView>
        <Grid
          containerStyle={{backgroundColor:sysColor.white, padding:5, margin:0, marginTop:20}}>
          <Col size={1}>
            <Avatar
                large
                source={{uri: sysUri.loadImg+"/"+this.state.profile.IMAGE}}
                onPress={() => this.props.navigation.navigate('ChangeIconView')}
                activeOpacity={0.7}
                containerStyle={{margin:10, padding: 0}}
              />
          </Col>
          <Col size={3} containerStyle={style.postContainer}>
            <Row><Text>{this.state.profile.NAME}</Text></Row>
            <Row><Text>{this.state.profile.PHONE}</Text></Row>
            <Row><Text>{this.state.profile.EMAIL}</Text></Row>
            <Row><Text style={{fontSize: 14, color:sysColor.deepGray}}>{this.state.profile.ADDRESS}</Text></Row>
          </Col>
        </Grid>

      


        <List containerStyle={{padding: 0, margin: 0, marginTop: 20}}>
            <ListItem
                key={1}
                hideChevron={true} 
                switchButton={true}
                switched={true}
                onSwitch={() => {}} 
                title={'Hidden Mobile Number'}
                />
            <ListItem
                key={2}
                hideChevron={true} 
                switchButton={true}
                switched={false}
                onSwitch={() => {}} 
                title={'Hidden Email'}
                
                 />
        </List>


        <List containerStyle={{padding: 0, margin: 0, marginTop: 20}}>
            <ListItem
                key={1}
                roundAvatar
                title={'My Points: 1'}
                leftIcon={{name: 'attach-money', color:sysColor.deepGray}} 
                onPress = {() => this.props.navigation.navigate('AddPoints')}
                />
            <ListItem
                key={2}
                roundAvatar
                title={'My Posts'}
                leftIcon={{name:'photo-library', color:sysColor.deepGray}}
                onPress = {() => this.props.navigation.navigate('UserProfile', 
                    { idUser:this.state.profile.IDUSER, 
                      name: this.state.profile.NAME, 
                      profile: this.state.profile,} )}
                  />
            <ListItem
                key={3}
                roundAvatar
                title={'Favorites'}
                leftIcon={{name:'favorite-border', color:sysColor.deepGray}}
                onPress = {() => this.props.navigation.navigate('UserProfile', 
                    { idUser:this.state.profile.IDUSER, 
                      name: this.state.profile.NAME, 
                      type: 'MYFAV',
                      profile: this.state.profile,} )}
                  />
        </List>
        {console.log(this.state.profile)}
        {this.state.profile.IDSHOP != null ? 
            <List containerStyle={{padding: 0, margin: 0, marginTop: 20, marginBottom: 20}}>
                <ListItem
                    key={2}
                    roundAvatar
                    title={'My Shop'}
                    leftIcon={{name:'store', color:sysColor.deepGray}}
                    />
            </List>
        
        : 
          <Text></Text>
        
        }

        <List containerStyle={{padding: 0, margin: 0, marginTop: 20, marginBottom: 20}}>
            <ListItem
                key={2}
                hideChevron={true} 
                roundAvatar
                title={'Clean Cache'}
                leftIcon={{name:'settings', color:sysColor.deepGray}}
                />
        </List>

        <Button 
          containerStyle={{padding: 0, margin: 0}}
          backgroundColor= {sysColor.pink}
          borderRadius={5}
          title="Sign Out"
          onPress = {() => this.signout()}
        />
    
      </ScrollView>
    );
    }
  }

  renderSignonSignup(){
    return (
      <View style={style.cardDisplay}>
        <Text style={{
          fontSize: 20,
          textAlign: "center",
          marginTop: 10,
        }}>
          Signon Or Signup
        </Text>
        <Divider style={{margin: 5}}/>
        <Text style={{
          fontSize: 16,
          textAlign: 'left',
          marginTop: 10,
        }}>
          Mobile Numer:
        </Text>
        <TextInput
          style={{height: 40, borderColor: sysColor.gray, borderBottomWidth: 1, margin: 5}}
          onChangeText={(text) => this.handleChange(text)}
          value={this.state.text}
        />
        
        <Text  style={{
          fontSize: 16,
          textAlign: 'left',
          marginTop: 20,
        }}>
            Code Received:
          </Text>
        <View style={{flex: 1, flexDirection:'row', flexWrap:'wrap', margin: 5}}>
          <TextInput
            style={{ width: 150, height: 40, borderColor: sysColor.gray, alignItems:'flex-end', borderBottomWidth: 1, paddingRight: 5}}
            onChangeText={(text) => this.setState({code: text})}
            value={this.state.code}
        />
        <Button 
          onPress = {() => this.getCode()}
          title="Get Code"
          accessibilityLabel="Get promotion code to sign in"
          backgroundColor= {sysColor.green}
          borderRadius ={5}
        />
        </View>
        <Divider style={{marginTop: 60}}/>  
      </View>
    );
  }
}