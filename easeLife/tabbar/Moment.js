
import React from 'react';
import {  StyleSheet, NetInfo, Text, View, ScrollView, FlatList, Image, AsyncStorage, TouchableOpacity, Dimensions} from 'react-native';
import ProtoPostOperate from '../prototype/ProtoPostOperate';
import { Button, Tabs, Tab, Icon, Header,SearchBar, Divider} from 'react-native-elements';
import {
  TabNavigator,
  StackNavigator
} from 'react-navigation';
import { Map as iMap} from 'immutable';


import NewPostView from '../moment/NewPostView';
import style from '../static/style';
import sysColor from '../config/SysColor';
import ElementView from '../moment/ElementView';

import FetchJson from '../api/FetchJson';
import sysUri from '../config/SysUri';
import CacheUser from '../cache/CacheUser';
import cachePost from '../cache/cachePost';
import PubSub from 'pubsub-js';


export default class Moment extends ProtoPostOperate {
  constructor() {
    super();
 
    this.state = {
      posts: cachePost,
      isLoading: true,
      refreshing: false,
      error: false,
      network: false,
      erorInfo: "",
      profile: {},
      clickStyle: {color: sysColor.red, fontWeight:'bold', fontSize:18 },
      currentPage: 0,
      scrollPoint: {x: 0, y:0, animated: true},
      myFavorite: new Map(),
      ad: new Map(),
      views: new iMap([['All', new Map()], 
                      ['News', new Map()],
                      ['Food', new Map()], 
                      ['Rent', new Map()], 
                      ['Car', new Map()],
                      ['Job', new Map()],
                      ['Edu', new Map()],
                      ['Immi', new Map()]])

    }
    this.onAnimationEnd = this.onAnimationEnd.bind(this)
    this.changeEle = this.changeEle.bind(this)
   
  }
  


  loadPosts(action, type, currentIdPost){
    console.log("Loading post ...");

    var uri = ''
    
    if(action.toUpperCase() === 'INIT'){
      uri = sysUri.initPost
    }else if(action.toUpperCase() === 'MORE'){
      uri = sysUri.morePost+'/'+type.toUpperCase() + '/'+currentIdPost
    }else if(action.toUpperCase() === 'REFRESH'){
      uri = sysUri.freshMorePost+'/'+type.toUpperCase() + '/'+currentIdPost
    }
    console.log(uri)
    fetch(uri).then((res) => {
      console.log(res)
      return res.json();
    }).then((res)=>{
      res.map((item, index) => {
        var categories = item.CATEGORY.split(',')
        this.state.views.get('All').set(item.IDPOST,{post: item, key:item.IDPOST})
        cachePost.set(item.IDPOST, item)
        categories.map((ca, i) =>{
          switch(ca.toUpperCase()){
            case 'FOOD': 
              var tempMap = this.state.views.get('Food').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'RENT': 
              var tempMap = this.state.views.get('Rent').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'CAR':  
              var tempMap = this.state.views.get('Car').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'NEWS': 
              var tempMap = this.state.views.get('News').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'JOB': 
              var tempMap = this.state.views.get('Job').set(item.IDPOST,{post: item, key:item.IDPOST}) 
              break;
            case 'EDU': 
              var tempMap = this.state.views.get('Education').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'IMMI': 
              var tempMap = this.state.views.get('Immigrant').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            default: break;
          }
        })
      });
      
      // console.log([...this.state.views.get('All').values()])
      this.setState({
        isLoading: false,
        refreshing: false,
      })

    }).catch((error) => {
      this.setState({
        isLoading: false,
        refreshing:false,
      })
      console.error(error);
    });
  }

  handleConnectionChange =  async (isConnected) => {
    this.setState({ network: isConnected });
    console.log(this.state.network)
    if(this.state.network)  {
      console.log(this.state.network)
      await this.signonCheck()
      await this.loadFavorite()
      await this.loadAD()
      await this.loadPosts('INIT','',-1)
    }else{
      console.log(this.state.network)
      [cachePost.values()].map(item =>{
        var categories = item.CATEGORY.split(',')
        categories.map(ca => {
          switch(ca.toUpperCase()){
            case 'FOOD': 
              var tempMap = this.state.views.get('Food').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'RENT': 
              var tempMap = this.state.views.get('Rent').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'CAR':  
              var tempMap = this.state.views.get('Car').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'NEWS': 
              var tempMap = this.state.views.get('News').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'JOB': 
              var tempMap = this.state.views.get('Job').set(item.IDPOST,{post: item, key:item.IDPOST}) 
              break;
            case 'EDU': 
              var tempMap = this.state.views.get('Education').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            case 'IMMI': 
              var tempMap = this.state.views.get('Immigrant').set(item.IDPOST,{post: item, key:item.IDPOST})
              break;
            default: break;
          }
        })
      
        this.setState({
          networkInfo: 'Your network is not connected ...',
          isLoading: false,
          refreshing: false,
        })
    })
    }
  }
  async componentWillMount(){
    NetInfo.isConnected.removeEventListener('change', this.handleConnectionChange);
    // await this.loadPosts()
  }
  
  async componentDidMount(){
    var token = PubSub.subscribe( 'Signout', (msg, data) => {
      this.setState({
        profile:{},
        myFavorite: new Map(),
      }) 
      this.loadPosts('INIT','',-1)
    } );

    var logon = PubSub.subscribe( 'Logon', async (msg, data) => {
      await this.signonCheck()
      await this.loadFavorite()
      await this.loadAD()
      this.loadPosts('INIT', '' , -1)
    } );
    
    NetInfo.isConnected.addEventListener('change', this.handleConnectionChange);
    NetInfo.isConnected.fetch().done(
      (isConnected) => { 
        console.log(this.state.network)
        this.setState({ network: isConnected }); 
       
      }
    );

    // await this.loadAD()
    if(this.state.network){
      await this.signonCheck()
      await this.loadFavorite()
      await this.loadAD()
      await this.loadPosts('INIT','',-1)
      
    }else{
      this.setState({
        networkInfo: 'Your network is not connected ...',
        isLoading: false,
      })
    }
  }
  

  // 监听滚动
  onAnimationEnd(e){
      // 求出水平方向上的偏移量
      var offSetX = e.nativeEvent.contentOffset.x;
      // 计算当前页码
      // console.log(offSetX)
      var currentPage = offSetX / Dimensions.get('window').width;
      // console.log(currentPage)
      // 重新绘制UI
      this.setState({
          currentPage:currentPage
      });
  }
  onAnimationBegin(e){
    // // 求出水平方向上的偏移量
    // var offSetX = e.nativeEvent.contentOffset.x;
    // // 计算当前页码
    // console.log(offSetX)
    // var currentPage = offSetX / Dimensions.get('window').width;
    // console.log(currentPage)
    // // 重新绘制UI
    // this.setState({
    //     currentPage:currentPage
    // });
  }
 
 


  renderItem({item, index}){
    // console.log(item)
    // console.log(this.state.profile)
    // console.log(index)
    // console.log(this.state.profile.IDUSER==item.post.IDUSER)
    if(item.key%3 == 0){
      // console.log(this.state.ad)
      // console.log(item.key/3)
      if(this.state.ad.size>0){
        return (<View>
          <ElementView post={item.post} key={item.key} 
              navigation={this.props.navigation} 
              profile={this.state.profile} 
              myFavorite={this.state.myFavorite.get(item.post.IDPOST)} 
              changeEle = {this.changeEle} />
          <TouchableOpacity style={{width: Dimensions.get('window').width-10, height: 80, margin: 2, marginLeft: 5, marginRight:5}} 
              onPress={() =>{this.props.navigation.navigate('ADWeb', {link:this.state.ad.get(parseInt(item.key/3)).LINK})}}>
            <Image style={{width: Dimensions.get('window').width-10, height: 80, margin: 2, marginLeft: 5, marginRight:5}} 
              source={{uri: sysUri.loadImg+'/'+this.state.ad.get(parseInt(item.key/3)).IMAGE}} 
              />
          </TouchableOpacity>
          <Divider style={style.devider} />
        </View>)
      }
    }
    return <ElementView post={item.post} key={item.key} 
        navigation={this.props.navigation}
        profile={this.state.profile} 
        myFavorite={this.state.myFavorite.get(item.post.IDPOST)}
        changeEle = {this.changeEle} 
      />
  }


  render() {
    // const { navigate } = this.props.navigation;
    // console.log(this.state.isLoading, this.state.error, this.state.network)
    if (this.state.error) {
      return this.renderErrorView(this.state.errorInfo);
    } else if(this.state.isLoading){
      return this.renderLoadingView();
    }
    return (
      <View>
        <ScrollView 
          horizontal={true}
          height={32}
        >
        {[...this.state.views.keys()].map((item, i) => {
          if(this.state.currentPage==i){
            return <View style={{borderBottomWidth:2,borderBottomColor:sysColor.green}}
              key={i}>
              <Text style={[style.scrollLabel, this.state.clickStyle]} 
                onPress={()=>{  
                  this.setState({currentPage: i, scrollPoint: {x:i*Dimensions.get('window').width, y:0, animated: true}})
                  this.refs.contentScrollView.scrollTo({x:i*Dimensions.get('window').width, y:0, animated: true})
                  }
                }>
                {item}
              </Text></View>
          }else{
            return <View  
              key={i}>
              <Text style={[style.scrollLabel]} 
                onPress={()=>{
                  {/* console.log(i*Dimensions.get('window').width) */}
                  this.setState({currentPage: i, scrollPoint: {x:i*Dimensions.get('window').width, y:0, animated: true}})
                  this.refs.contentScrollView.scrollTo({x:i*Dimensions.get('window').width, y:0, animated: true})
                  }
                  
                }>{item}
              </Text></View>
          }
        })}

        </ScrollView>
        {this.state.network ===false ? this.renderNetworkInfo(this.state.networkInfo) :  <Text></Text>}
        <ScrollView  style={{ 
              backgroundColor: sysColor.white, }} 
             horizontal={true}
             pagingEnabled={true}
             onMomentumScrollEnd={this.onAnimationEnd}
             onMomentumScrollBegin={this.onAnimationBegin}
             ref = 'contentScrollView'
             >
          {/* {console.log([...this.state.views.keys()])} */}
          {[...this.state.views.keys()].map((key) =>{
            {/* var data = [...this.state.views.get(key).values()].slice().sort(function(a,b){return b.key-a.key}) */}
            return this.renderFlatList(key)
          })}
            
        </ScrollView>
      </View>
    )
  }

  renderFlatList(type){
    // console.log(type)
    // console.log(this.state.isLoading)
    // console.log([...this.state.views.get(type).values()])
    return (<FlatList 
      style={{width: Dimensions.get('window').width, }}
      key={type}
      data = {[...this.state.views.get(type).values()].slice().sort(function(a,b){return b.key-a.key})}
      renderItem={(item) => this.renderItem(item)}
      ListFooterComponent={this.renderLoadingView()}
      //ListHeaderComponent={this.renderLoadingView()}
      refreshing={this.state.refreshing}
      onRefresh={() => this.handleRefresh(type)}
      //onEndThreshold={1}
      onEndReachedThreshold={0}
      onEndReached={() => this.handleLoadMore(type)}
      onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
    />)
}

handleRefresh (type) {
    this.setState({
      refreshing:true,
    }, this.loadPosts('REFRESH', type, Math.max.apply(null,[...this.state.views.get(type).keys()])))
  }
  
  handleLoadMore(type) {
    // console.log([...this.state.views.get(type).keys()])
    if(!this.onEndReachedCalledDuringMomentum)
    {
      this.loadPosts('MORE',type, Math.min.apply(null,[...this.state.views.get(type).keys()]))
      this.onEndReachedCalledDuringMomentum = true;
    }
  }






  static navigationOptions = ({navigation, screenProps}) => ({
      // headerLeft: <Icon
      //               name='search'
                    
      //               color={sysColor.white} 
      //               onPress={() => navigation.navigate("Search")}
      //               containerStyle = {{margin: 5, marginRight: 10}} />,
      headerRight:  <Icon
                name='photo-camera'
                
                color={sysColor.white}
                onPress={() =>
                  navigation.navigate('NewPostView')
                }
                containerStyle = {{margin: 5, marginRight: 10}}
              />, 
      // Note: By default the icon is only shown on iOS. Search the showIcon option below.
      tabBarIcon: ({ tintColor }) => 
        <Icon  color={tintColor} name='explore' size={25} />
      ,
  });

}

    //Search View      
/* <View 
                style={{height: 25, width: 250, padding: 1, 
                borderColor: sysColor.white, borderBottomWidth: 1,
                paddingTop: 5,
                margin:5,
                marginLeft: 10,
                flex: 1,
      
                flexDirection: 'row',
                }}
              >
              <Icon 
                small
                name="search"
                color={sysColor.white}
                containerStyle = {{margin: 5}}
              />
              <TextInput
                style = {{fontSize: 16,}}
                placeholder="Search ..."
                onChangeText={ () => {}}
                color= {sysColor.white}
              />
              </View> */