var sysColor = {
    red : '#ED3036',
    green1: '#99CC99',
    green2: '#4ca64c',
    green : '#1EB379',
    orange : '#FFB146',
    pink : '#FC5D5D',
    gray : '#D6D6D6',
    lightGray: '#f3f3f3',
    topGray: '#323232',
    deepGray : '#636363',
    black : '#000000',
    white: '#FFFFFF'
    
    
}
export default sysColor;