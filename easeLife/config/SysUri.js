let ip = '52.27.31.179'
// let ip = 'localhost'
let http='http://'+ip+':3000'
let https = 'http://'+ip+':3000'
const sysUri = {
    ws: 'ws://'+ip+':3000',
    wss: 'wss://'+ip+':3333',
    shop: https+'/services/ALLSHOPS',
    initPost: https+'/services/INITPOSTS',
    morePost: https+'/services2/MOREPOSTS',
    freshMorePost: https+'/services2/NEWPOSTS',
    loadImg: https+'/upload/IMG',
    allUsers: https+'/services/ALLUSERS',
    profileByPhone: https+'/users/signin',
    myPost: https+'/services1/MYPOSTS',
    myFriend: https+'/services2/MYFRIEND',
    newPost: https+'/upload/POST',
    uploadImage: https+'/upload/uploadImage',
    chatContent: https+'/services1/CHATCONTENT',
    AD: https+'/services/AD',
    myFavorite: https+'/services1/MYFAVORITE',
    myFavoritePost: https+'/services1/MYFAVORITEPOSTS',
}

export default sysUri;