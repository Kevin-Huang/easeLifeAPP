/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Button, Tabs, Tab, Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';
import {
  TabNavigator,
  StackNavigator
} from 'react-navigation';

import App from './App';


export default class easeLife extends Component {
  constructor() {
    super();
  }

  render(){
    return (
      <App />
    );
  }
}

AppRegistry.registerComponent('easeLife', () => App);