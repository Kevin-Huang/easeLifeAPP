import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import sysColor from '../config/SysColor';

const style = StyleSheet.create({
    avatarImg: {
        margin: 10,
    },
    title: {
        fontSize: 16,
        color: sysColor.deepGray,
        fontWeight: 'bold',
        padding: 5,
    },
    postImg: {
        width: 100,
        height: 90,
        margin: 3,
    },
    scrollLabel:{
        color: sysColor.deepGray,
        padding: 5,
        paddingBottom: 2,
        fontSize: 16,
        paddingLeft: 8,
        paddingRight: 8,
        
    },
    slide:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: sysColor.deepGray,
        // color: sysColor.white,
  },
    postContainer:{
        padding: 10,
        // marginLeft: 10,
       
    },
    postText:{
        fontSize: 16,
        padding: 5,
    },
    socialRow: {
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight:  15,
        marginTop: 5,
    },
    socialIcon: {
        // color: sysColor.deepGray,
        marginRight: 12
    },
    gap5:{
        margin: 5
    },
    cardDisplay:{
        backgroundColor: sysColor.white, 
        margin: 10, 
        padding: 20,
    },
    subtitleView:{
        flexDirection: 'row',
        paddingLeft: 10
    },
    ratingText:{
        color: 'grey',
        marginTop: 5,
        fontSize: 12,
    },
  bigblue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  red: {
    color: 'red',
  },
  devider: {
      backgroundColor: sysColor.gray,
      margin: 2,
  },
});

export default style;