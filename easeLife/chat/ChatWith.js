import React from 'react';
import { AppRegistry, StyleSheet, Platform, Text, View,ScrollView, ActivityIndicator,AsyncStorage } from 'react-native';
import { Button, Tabs, Tab, Icon, Header, List, ListItem  } from 'react-native-elements';
import {GiftedChat, Actions, Bubble, Message, Avatar} from 'react-native-gifted-chat';
import PubSub from 'pubsub-js';
import EaseLifeBase from '../prototype/EaseLifeBase';

// import CustomActions from './CustomActions';
import CustomView from './CustomView';

import sysColor from '../config/SysColor';
import sysUri from '../config/SysUri';
import style from '../static/style';

import CacheUser from '../cache/CacheUser';
// import instanceMessage from '../cache/instanceMessage';
import ChatStorage from './ChatStorage';
import Chat from '../tabbar/Chat';

export default class ChatWith extends EaseLifeBase {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
      profile: {},
      friend: {},
    };

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    // this.renderCustomActions = this.renderCustomActions.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this);
    this._isAlright = null;
  }

  static navigationOptions = ({ navigation }) => ({
   headerLeft:  <Icon name={'keyboard-arrow-left'} color={sysColor.white} 
        containerStyle={{padding:5}}
        onPress={ () => { 
        const {goBack} = navigation;
        if(navigation.state.params.resetNotify){
          navigation.state.params.resetNotify(navigation.state.params.currentTalkingFriend);
        }
        
        navigation.state.params.currentTalkingFriend=null;
        
        console.log("clean friend choosen")
        goBack(); } }  />
  });



  async componentWillMount() {
    // console.log(this.props.navitation)
    this._isMounted = true
    this.setState({
      profile: this.props.navigation.state.params.profile,
      friend: this.props.navigation.state.params.friend,
    })
      
  }

  componentDidMount(){
    this.setState({
      // profile: this.props.navigation.state.params.profile,
      // friend: this.props.navigation.state.params.friend,
      myObj: {
       // _id: this.props.navigation.state.params.IDUSER,
       // name: this.props.navigation.state.params.NAME,
         _id: this.state.profile.IDUSER,
         name: this.state.profile.NAME,
        avatar: sysUri.profile+'/'+this.state.profile.IMAGE
      },
      friendObj: {
         _id: this.props.navigation.state.params.friend.IDUSER,
        name: this.props.navigation.state.params.friend.NAME,
        avatar: sysUri.loadImg+'/'+this.props.navigation.state.params.friend.IMAGE
      }
    })
    console.log(this.state.profile.IDUSER+'-'+this.props.navigation.state.params.friend.IDUSER)
    ChatStorage.get(this.state.profile.IDUSER+'-'+this.props.navigation.state.params.friend.IDUSER, 
    (message) => {
      this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, message),
      }
    })
    })


    if(this.state.friend.IDUSER == this.props.navigation.state.params.currentTalkingFriend){
      // add the function to the list of subscribers for a particular topic
      // we're keeping the returned token, in order to be able to unsubscribe
      // from the topic later on
      var token = PubSub.subscribe( 'NewMessages', (msg, data) => {
        
        ChatStorage.store(this.state.profile.IDUSER+'-'+this.props.navigation.state.params.friend.IDUSER, [data.message])
        if (data.from == this.props.navigation.state.params.currentTalkingFriend){
          // ChatStorage.store(this.props.navigation.state.params.profile.IDUSER+'-'+this.props.navigation.state.params.friend.IDUSER, [data.message])
          this.setState((previousState) => {
            return {
              messages: GiftedChat.append(previousState.messages, data.message),
            }
          })
        }
      } );
    }  
  }


  onSend(messages = []) {
    console.log(messages)
    
    // ChatStorage.store(this.props.navigation.state.params.profile.IDUSER+'-'+this.props.navigation.state.params.friend.IDUSER, messages)
    messages.map(item =>{
      var message = item;
      message._id = Date.now()
      // console.log(message)
      
      if(message.image){
        console.log(message.image)
        var imageName = (Date.now()).toString()
        
         var formData = new FormData();
         formData.append('file',{uri: message.image, name: imageName, type: 'image/jpg'})
         formData.append('Content-Type',  'image/jpg');
         message.image = sysUri.loadImg + '/'+ imageName
         fetch(sysUri.uploadImage,{
            method: 'POST',
            headers: {
              'Content-Type':'multipart/form-data',
              'Accept': 'application/json',
            },
            body: formData
          }).then((status) => {
            console.log(status);
          }
          ).catch(error=>{
            console.error(error)
          })
        // var fileData = {name: "ttt.jpg", path: message.image}
        // this.ws.send(JSON.stringify(fileData));
        // this.ws.send(message.image);
      }
      this.setState((previousState) => {
        return {
          messages: GiftedChat.append(previousState.messages, message),
        };
      });
      var messageObj = {
          from: this.state.profile.IDUSER, to: this.state.friend.IDUSER,  message: message
      }
      console.log(messageObj)
      this.props.navigation.state.params.sendMessage(JSON.stringify(messageObj)); // send a message)
      // console.log(messageObj)
      ChatStorage.store(this.state.profile.IDUSER+'-'+this.props.navigation.state.params.friend.IDUSER,
       [message])
    })
    

  }   

  componentWillUnmount() {
    this._isMounted = false;
  }


  render() {
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={this.onSend}
        loadEarlier={this.state.loadEarlier}
        onLoadEarlier={this.onLoadEarlier}
        isLoadingEarlier={this.state.isLoadingEarlier}
        user={{
          _id: this.state.profile.IDUSER,
          name: this.state.profile.NAME,
          avatar: sysUri.loadImg+'/'+this.state.profile.IMAGE
        }}

        renderMessage={props => <CustomMessage {...props} />}

        
        renderBubble={this.renderBubble}
        renderCustomView={this.renderCustomView}
        renderFooter={this.renderFooter}
        
      />
    );
  }

 //renderActions={this.renderCustomActions} 

  onLoadEarlier() {
    this.setState((previousState) => {
      return {
        isLoadingEarlier: true,
      };
    });

    setTimeout(() => {
      if (this._isMounted === true) {
        this.setState((previousState) => {
          return {
            messages: GiftedChat.prepend(previousState.messages, require('./old_messages.js')),
            loadEarlier: false,
            isLoadingEarlier: false,
          };
        });
      }
    }, 1000); // simulating network
  }

  // renderCustomActions(props) {
  //   if (Platform.OS === 'ios') {
  //     return (
  //       <CustomActions
  //         {...props}
  //       />
  //     );
  //   }
  //   const options = {
  //     'Action 1': (props) => {
  //       alert('option 1');
  //     },
  //     'Action 2': (props) => {
  //       alert('option 2');
  //     },
  //     'Cancel': () => {},
  //   };
  //   return (
  //     <Actions
  //       {...props}
  //       options={options}
  //     />
  //   );
  // }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
         textStyle={{
          right:{
            color: sysColor.black,
          }
        }}
        wrapperStyle={{
          left: {
            backgroundColor: sysColor.white,
            borderRadius: 10,

          },
          right: {
            backgroundColor: sysColor.white,
            borderRadius: 10,
  
            
          }
        }} 
        bottomContainerStyle={{
          right: {
            width:0, height:0 
            },
          left: {
            width:0, height:0 
          }

        }}
      />
    );
  }

  renderCustomView(props) {
    return (
      <CustomView
        {...props}
      />
    );
  }

  renderFooter(props) {
    if (this.state.typingText) {
      return (
        <View style={styles.footerContainer}>
          <Text style={styles.footerText}>
            {this.state.typingText}
          </Text>
        </View>
      );
    }
    return null;
  }
}


const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
});

class CustomMessage extends Message {
  constructor(){
    super()
  }
  renderAvatar() {
    return (
      <Avatar {...this.getInnerComponentProps()} />
    );
  }
}
