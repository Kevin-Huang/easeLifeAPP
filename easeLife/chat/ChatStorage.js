import React from 'react';
import { AsyncStorage } from 'react-native';

let memeryChatStorage = new Map()
export default class ChatStorage{
    static async get(uidFid, callback){
        var result = memeryChatStorage.get(uidFid)
        // AsyncStorage.removeItem(uidFid).then(res=>console.log(res)).catch(error=>console.log(error))
        // AsyncStorage.removeItem('3-1').then(res=>console.log(res)).catch(error=>console.log(error))
        // AsyncStorage.removeItem('1-3').then(res=>console.log(res)).catch(error=>console.log(error))
        // console.log(result)
        if(result == null){
            await AsyncStorage.getItem(uidFid).then((res)=> {
                if (res !== null){
                    chatContentOrg =  JSON.parse(res)
                    // console.log('Reading from store...')
                    // console.log(chatContentOrg)
                    var chatContent = chatContentOrg.sort(function(a,b){
                        return b._id - a._id
                    })
                    // console.log(chatContentOrg.sort(function(a,b){
                    //     return b._id - a._id
                    // }))
                    memeryChatStorage.set(uidFid, chatContent)
                    callback(chatContent)
                }else{
                    callback([])
                }
            }).catch((error) => {
                console.error(error);
            });
        }else{
            // console.log('Reading from store...')
            // console.log(result)
            result = result.sort(function(a,b){
                return b._id - a._id
            })
            // console.log(result)
            callback(result)
        }
    }
    static async store(uidFid, newMessages){
        // await AsyncStorage.removeItem(uidFid);
        var temp = memeryChatStorage.get(uidFid)
        // console.log(uidFid)
        // console.log(temp)
        console.log('store data:'+uidFid +":"+newMessages)
        if(temp == null){
            memeryChatStorage.set(uidFid,newMessages)
            await AsyncStorage.setItem(uidFid, JSON.stringify(newMessages)).then(res=>{
                console.log(res)
            }).catch(error=>{
                console.error(error)
            })
        }else{
            concatResult = temp.concat(newMessages)
            // console.log(concatResult)
            memeryChatStorage.set(uidFid,concatResult)
            await AsyncStorage.setItem(uidFid, JSON.stringify(concatResult)).then(res=>{
                console.log(res)
            }).catch(error=>{
                console.error(error)
            })
        }
    }
}