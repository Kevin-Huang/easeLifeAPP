import React from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList, Image, AsyncStorage, Dimensions, TouchableOpacity,} from 'react-native';
import { Badge, Grid, Col, Row, Button, Tile,Avatar, Divider, Card, List, ListItem } from 'react-native-elements';
import ProtoPostOperate from '../prototype/ProtoPostOperate';

import ElementView from '../moment/ElementView';

import CacheUser from '../cache/CacheUser';

import sysColor from '../config/SysColor';
import style from '../static/style';
import sysUri from '../config/SysUri';


export default class UserProfile extends ProtoPostOperate {
  constructor() {
    super();
    this.state = {
      posts: [],
      isLoading: true,
      refreshing: false,
      error: false,
      errorInfo: "",
      profile: {},
      userProfile: {},
      myFavorite: new Map()
    }
    this.changeEle = this.changeEle.bind(this)
  }

  componentWillMount(){
    console.log(this.props.navigation.state)
    this.setState({
      profile:this.props.navigation.state.params.profile
    })
      this.loadFavorite(this.props.navigation.state.params.idUser)

      CacheUser.getUser(this.props.navigation.state.params.idUser).then(res =>{
        // console.log(res)
        // res2 = res.json()
        // console.log(res)
        this.setState({
          userProfile:res
        })
    }).catch(error => console.log(error));
  }

loadPosts(){
  //  console.log(sysUri.myPost+"/"+this.props.navigation.state.params.idUser)
  var uri = ''
  if(this.props.navigation.state.params.type==='MYFAV'){
    uri = sysUri.myFavoritePost+"/"+this.props.navigation.state.params.idUser
  }else{
      uri = sysUri.myPost+"/"+this.props.navigation.state.params.idUser
  }
  fetch(uri).then((res) => {
    if(res.headers.get("content-length") > 0){
      console.log(res)
      res.json().then( async (json) => {
        var newState = this.state.posts;
        
        json.map((item, index) => newState.push({post: item, key:item.IDPOST}));
        this.setState({
            posts: newState,
            isLoading: false,
            error: false,
            errorInfo: "No error",
        });
   }).catch(err => console.log(err))
  }else{
    this.setState({
      isLoading: false,
      error: true,
      errorInfo: "No Post Find",
  });
   }
  }).catch((error) => {
    this.setState({
      error: true,
      isLoading: false,
      errorInfo: error,
    })
    console.error(error);
  });
}

 async componentDidMount(){
  //  console.log(this.state.posts);
  await this.loadPosts()
  var token = PubSub.subscribe( 'Signout', async (msg, data) => {
      this.setState({
        profile:{},
        userProfile: {},
        myFavorite: new Map(),
      }) 
      await this.loadPosts()
    } );

  var logon = PubSub.subscribe( 'Logon', async (msg, data) => {
    await this.loadFavorite(this.props.navigation.state.params.idUser)
    await this.loadAD()
    await this.loadPosts()
  } );

  }

  renderFlatList(data){
    // console.log(type)
    // console.log(this.state.isLoading)
    // console.log([...this.state.views.get(type).values()])
    return (<FlatList 
      style={{width: Dimensions.get('window').width, }}
      
      //data = {[...this.state.views.get(type).values()].slice().sort(function(a,b){return b.key-a.key})}
      data = {data}
      renderItem={(item) => this.renderItem(item)}
      //ListFooterComponent={this.renderLoadingView()}
      //ListHeaderComponent={this.renderLoadingView()}
      //refreshing={this.state.refreshing}
      //onRefresh={() => this.handleRefresh('ALL')}
      //onEndThreshold={1}
      //onEndReachedThreshold={0}
      //onEndReached={() => this.handleLoadMore('ALL')}
      //onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
    />)
}

  render() {
    if(this.state.isLoading) return this.renderLoadingView()
    if(this.state.error) return this.renderErrorView(this.state.errorInfo)
    return (
      <ScrollView>
        {this.state.profile.IDUSER != this.state.userProfile.IDUSER ?
          <View>
            <Grid containerStyle={{backgroundColor:sysColor.white, padding:5, margin:0, marginTop:20, marginBottom: 20}}>
              <Col size={1}>
                <Avatar
                    large
                    source={{uri: sysUri.loadImg+"/"+this.state.userProfile.IMAGE}}
                    containerStyle={{margin:0, padding: 0}}
                  />
              </Col>
              <Col size={3} containerStyle={style.postContainer}>
                <Row><Text containerStyle={style.title}>{this.state.userProfile.NAME}</Text></Row>
                <Row><Text>{this.state.userProfile.EMAIL}</Text></Row>
                <Row ><Text>{this.state.userProfile.PHONE}</Text></Row>
              </Col>
            </Grid>
            <Button 
              containerStyle={{padding: 0, margin: 0, marginTop: 10}}
              backgroundColor= {sysColor.green}
              borderRadius ={5}
              title = "Send Message"
              onPress = {() => {
                    this.props.navigation.navigate('ChatWith', {profile: this.state.profile, friend: this.state.userProfile,friendName:this.state.profile.NAME})
                  }}
            />
          </View>
          :
          <Text></Text>
        }

        <Card title={this.props.navigation.state.params.name + '\'s Post'} titleStyle={{padding: 3}} containerStyle={{padding: 0, margin: 5, marginTop: 10,}}> 
          {this.renderFlatList(this.state.posts, 'All', this.state.refreshing)}
        </Card> 
    </ScrollView>
    )
  }

  renderItem({item}){
    // console.log(item.post)
    return <ElementView 
              post={item.post} key={item.key}  
              navigation={this.props.navigation}
              profile={this.state.profile} 
              myFavorite={this.state.myFavorite.get(item.post.IDPOST)} 
              changeEle = {this.changeEle}/>
  }

}