import React from 'react';
import { StyleSheet, Text, TextInput, Button, View, TouchableHighlight,ScrollView, Image, AsyncStorage, Linking} from 'react-native';
import { Badge, Grid, Col, Row, Tile,Avatar, Divider, Card, List, ListItem } from 'react-native-elements';
import FetchJson from '../api/FetchJson';


import sysColor from '../config/SysColor';
import style from '../static/style';

export default class AddPoints extends React.Component  {
  constructor() {
    super();

  }
  openUrl(url){
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }
 
  render() {
    return (
      <View style={style.cardDisplay}> 
        <View style={{fontSize: 22}}>
        <Text style={{fontSize: 22}}>Please email </Text>
        <TouchableHighlight activeOpacity={ 0.75 } onPress={() => this.openUrl('mailto:info@novasoftware.com.au?subject=Add More EaseLife Points')}>
          <Text style={{color:sysColor.green,fontSize: 22}}>info@novasoftware.com.au</Text>
        </TouchableHighlight>
        <Text style={{fontSize: 22}}> or call </Text>
        <TouchableHighlight activeOpacity={ 0.75 } onPress={() => this.openUrl('tel:+61452542687')}>
          <Text style={{color:sysColor.green, fontSize: 22}}>+61452542687</Text> 
        </TouchableHighlight>
        <Text style={{fontSize: 22}}>to add more points</Text>
        </View>
      </View>);
  }
}