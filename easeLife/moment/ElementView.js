import React from 'react';
import { StyleSheet, TouchableOpacity, Text, ScrollView, View, Image } from 'react-native';
import { Badge, Grid, Col, Row, Avatar, Icon, Divider } from 'react-native-elements';

import CacheUser from '../cache/CacheUser';

import sysColor from '../config/SysColor';
import style from '../static/style';
import sysUri from '../config/SysUri';


export default class ElementView extends React.Component {
  constructor() {
    super();
  }


  pressImage(index, images, desc){
    this.props.navigation.navigate("ImageSwiper", {index: index, images: images, desc: desc})
  }

  share(idPost){

  }

  addFavorite(post){
    this.props.changeEle('ADDFAV', post)
  }
  delFavorite(post){
    this.props.changeEle('DELFAV', post)
  }
  renewPost(idPost){
    this.props.changeEle('RENEWPOST', idPost)
  }
  deletePost(idPost){
    this.props.changeEle('DELETEPOST', idPost)
  }

  render() {
    var post = this.props.post;
    // console.log(post.URLIMAGES);
    var images = post.URLIMAGES.slice(1, -1).split(',');
    // console.log(this.props.myPost)
    // console.log(this.props.myFavorite)
    // console.log(this.props.myFavorite == undefined)
    // console.log(Users.get(1).IMAGE);
    return (
      <View>
        <Grid
          containerStyle={{margin:0, marginTop:5}}>
          {/* <Col size={1}>
            <Avatar 
                containerStyle={style.avatarImg}
                medium
                source={{uri: sysUri.loadImg+'/'+post.IMAGE}}
                onPress={() => this.props.navigation.navigate('UserProfile', {idUser: post.IDUSER, name: post.USERNAME})}
                activeOpacity={0.7}
              />
          </Col> */}
          <Col containerStyle={style.postContainer}>
            {/* <Row><Text style={style.title} >
                    {post.NAME}
                </Text></Row> */}
            <Row><Text style={style.postText}>{post.CONTENT}</Text></Row>
           
            <Row >
               <View style={{flex:1, flexDirection: 'row', flexWrap: 'wrap', marginLeft:0}}>
                 {
                  images.map((item, index ) => {
                   {/*console.log(item);*/}
    
                   var imgUri = sysUri.loadImg+'/'+item.trim().replace(/"/g, '');
                   {/*console.log(imgUri);*/}
                   return (
                      <TouchableOpacity 
                        activeOpacity={ 0.75 }
                        style={style.postImg} 
                        onPress = {() => this.pressImage(index, images, post.CONTENT)}
                        key = {index}
                        >
                        <Image style={style.postImg} source={{uri: imgUri}} />
                      </TouchableOpacity>
                   );
                 })}
             </View>
            </Row>

            {post.IDQUICKORDER >= 1? <View
              style={{margin:5, padding: 10, backgroundColor:sysColor.lightGray }}>
              <Text onPress={()=> null}>{post.NAME}</Text>
              </View> : null}
            
             {post.IDARTICLE >= 1? <View
                style={{flex:1, flexDirection: 'row',margin:5, padding: 5, backgroundColor:sysColor.lightGray }}>
                
                <Avatar 
                  small
                  source={{uri: sysUri.loadImg+'/'+post.ARTICLEIMG}}
                  onPress={() =>{this.props.navigation.navigate('ADWeb', {link:post.ARTICLEURI})}}
                />
                <Text style={{marginTop:5, marginLeft: 5, padding: 5}}
                  onPress={() =>{this.props.navigation.navigate('ADWeb', {link:post.ARTICLEURI})}}>
                    {post.ARTICLENAME}</Text>
              </View> : null}

            <Row>
              <View style={{flex: 1,flexDirection: 'row', justifyContent: 'space-between', marginTop:3}}>
              <View>
                <Text style={{fontSize:12, marginLeft: 15, marginTop: 5, color: sysColor.deepGray}}
                  onPress={() => this.props.navigation.navigate('UserProfile', 
                    { idUser: post.IDUSER, 
                      name: post.USERNAME, 
                      profile: this.props.profile,})}>
                  by {post.USERNAME}
                </Text>
              </View>
              <View style={style.socialRow}>
              {/* {this.props.myPost ? console.log("myPost: true"): console.log("myPost: false")} */}
                {this.props.profile.IDUSER == post.IDUSER ?  <Icon name='autorenew' containerStyle={style.socialIcon} size={18} color={sysColor.deepGray} 
                      onPress={() => this.renewPost(post.IDPOST)}
                    /> : <Text></Text>}
                {this.props.profile.IDUSER == post.IDUSER ?  <Icon name='delete' containerStyle={style.socialIcon}  size={18} color={sysColor.deepGray} 
                      onPress={() => {
                        console.log("delete was pressed")
                        this.deletePost(post.IDPOST)
                        }}
                    /> : <Text></Text>}
                {this.props.myFavorite == undefined ? <Icon name='favorite-border' containerStyle={style.socialIcon}  size={18} color={sysColor.deepGray} 
                      onPress={() => this.addFavorite(post)}/>
                  : <Icon name='favorite' containerStyle={style.socialIcon}  size={18} color={sysColor.deepGray} 
                      onPress={() => this.delFavorite(post)} />}
                <Icon name='share' containerStyle={style.socialIcon}  size={18} color={sysColor.deepGray} 
                  onPress={() => this.share(post.IDPOST)}/>
                
                {this.props.profile.IDUSER != post.IDUSER ? 
                  <Icon name='forum' containerStyle={style.socialIcon}  size={18} color={sysColor.deepGray} 
                  onPress={() => {
                    CacheUser.getUser(post.IDUSER).then(friend => {
                      this.props.navigation.navigate('ChatWith', {profile:this.props.profile, friend: friend,friendName:friend.NAME})
                    })
                  }}/>
                  : <Text></Text>
                }
                
              </View>
              </View>
            </Row>
          </Col>

        </Grid>
      
        <Divider style={style.devider} />
      </View>
    )
  }
}

