import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import { Badge, Grid, Col } from 'react-native-elements';
import Swiper from 'react-native-swiper';


import sysColor from '../config/SysColor';
import style from '../static/style';
import sysUri from '../config/SysUri';




export default class ImageSwiper extends React.Component {
  constructor() {
    super();
    
  }

  static navigationOptions = {
    title: '',
    headerStyle: {
        backgroundColor: 'transparent',
        position: 'absolute',
        height: 50,
        top: 0,
        left: 0,
        right: 0,
        // color: sysColor.white,
      },
  };

  render() {

    let {index, images, desc} = this.props.navigation.state.params;

    const renderPagination = (index, total, context) => {
      console.log(context)
      return (
        <View style={{position: 'absolute',
                      backgroundColor: sysColor.deepGray,
                      width: Dimensions.get('window').width-20,
                      height: 80,
                      bottom: 20,
                      right: 10, left: 10}}>
          <Text style={{ color: 'white', fontSize: 20, textAlign:'center' }}>
            <Text >{index + 1 }</Text>/{total}
          </Text>
          <Text style={{ color: 'white', fontSize:20 }}>{desc}
          </Text>
        </View>
      )
    }
    console.log(Dimensions.get('window').width);
    return (
      <Swiper  
        index={index}
        renderPagination={renderPagination}
        >
        {images.map((image, index)=>{
            console.log(sysUri.loadImg+"/"+image.trim());
            return (<View key={index} style={style.slide} title={<Text numberOfLines={1} style={{color:sysColor.red}}>Something</Text>}>
                <Image source={{uri: sysUri.loadImg+"/"+image.trim().replace(/"/g, '')}} 
                    style={{width: Dimensions.get('window').width, height:  Dimensions.get('window').height }}
                    resizeMode={'contain'} 
        />
                </View>)
        })}
      </Swiper>
    )
  }
}