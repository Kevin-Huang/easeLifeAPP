import React from 'react';
import {  View } from 'react-native';

import { SearchBar, Divider } from 'react-native-elements';
import cachePost from '../cache/cachePost';

import sysColor from '../config/SysColor';
import style from '../static/style';

export default class Search extends React.Component {
  constructor() {
    super();
    this.state = {
      searchText: '',
      posts:new Map()
    }
    this.cachePost = cachePost;
    this.handleChange = this.handleChange.bind(this);
  }
  static navigationOptions = {
    title: 'Search',
  };

  handleChange(text) {
    this.setState({searchText: text});
    var matchPosts = new Map()
    for (var item of myMap.values()) {
      var longstring = item.CONTENT+item.CATEGORY+item.NAME+item.USERNAME+item;
      if(longstring.Search(text)){
        matchPosts.set(item.IDPOST, item)
      }
    }
    this.setState({
      posts: matchPosts
    })
  }

  handleKeyDown(e) {
    if(e.nativeEvent.key == "Enter"){
        dismissKeyboard();
    }
  }

  render() {
    // console.log(this.props.navigation.state.params)
    return (
      <View>
        <SearchBar
          lightTheme
          onChangeText={this.handleChange}
          keyboardType="default"
          returnKeyType="search"
          autoFocus={true}
          value={ this.state.searchText }
          onKeyPress={this.handleKeyDown}
          placeholder='Type Here...' />
        
          

      </View>
    )
  }
}