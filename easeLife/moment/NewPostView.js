import React from 'react';
import { StyleSheet, Text, TextInput, ScrollView, View, Image,AsyncStorage, NativeModules , DeviceEventEmitter, TouchableOpacity, Alert} from 'react-native';
import { Badge, Grid, Col, Row, Avatar, Icon, Divider,Button } from 'react-native-elements';
import {
  StackNavigator,
} from 'react-navigation';

import sysColor from '../config/SysColor';
import style from '../static/style';
import sysUri from '../config/SysUri';


var ImagePicker = NativeModules.ImageCropPicker;
var choose = true;



export default class NewPostView extends React.Component {
  constructor() {
    super();
    this.state = {
      value: 'Please write a post here ... ',
      images: [],
      profile: null,
      error: false,
      errorInfo: "",
      posting: false,
    };
  }

static navigationOptions = ({navigation, screenProps}) => ({
      title: '发布',
      tabBarIcon: ({ tintColor }) => (
        <Icon  color={tintColor} name='explore' size={25} />
      ),
  });

  async componentDidMount(){
     await AsyncStorage.getItem('Profile').then((profile)=> {
        if (profile !== null){
            this.setState({
              profile: JSON.parse(profile),
              error: false,
            })
        }
      }).catch((error) => {
          this.setState({
            error:true,
            errorInfo:"Local profile loading error ...",
          })
          console.error(error);
        });
    if (this.state.profile==null){
      Alert.alert(
        'Signin/Signup',
        'Posting requires an Ease Life account. You can Signin/Signup now or Cancel and back to Moment',
        [
          {text: 'Signin/Signup', onPress: ()=> {this.props.navigation.navigate('Profile')}},
          {text: 'Cancel', onPress: ()=> {this.props.navigation.navigate('Moment')}}
        ],
        {cancelable: false}
      );
    }
  }


  handleChange(text) {
      this.setState({value: text});
    }

  handleSubmit() {
    // alert('An essay was submitted: ' + this.state.value);
    // event.preventDefault();
    this.setState({
      posting: true,
    });
    

    var formData = new FormData();
    
    // imageArray = [];
    // this.state.images.map(item => {
    //   formData.append('file',{uri: item.path, type:item.mime, name: this.state.profile.IDUSER})});
    for(var i=0; i<this.state.images.length; i++){
      formData.append('file', {uri: this.state.images[i].path, name: (Date.now()).toString(), type: this.state.images[i].mime});
    }
  // //  {uri: this.state.images[0].path, name: this.state.images[0].parse, type: this.state.images[0].mime}
    // formData.append('file', {uri: this.state.images[0].path, name: this.state.images[0].path, type: this.state.images[0].mime});
    // formData.append('file', {uri: this.state.images[1].path, name: this.state.images[1].path, type: this.state.images[1].mime});
    
    // formData.append('files',imageArray);
    formData.append('Content-Type',  this.state.images[0].mime);
    formData.append('idUSER', this.state.profile.IDUSER);
    formData.append('userName', this.state.profile.NAME);
    formData.append('content', this.state.value);
    formData.append('idSHOP', this.state.profile.IDSHOP);
    formData.append('name', null);
    formData.append('category', null);
    formData.append('numLike', 0);
    formData.append('urlImages', null);
    formData.append('dateTime', Date().toString());
    formData.append('idQUICKORDER', null);
    console.log(formData)

    fetch(sysUri.newPost,{
      method: 'POST',
      headers: {
        'Content-Type':'multipart/form-data',
        'Accept': 'application/json',
      },
      body: formData
    }).then((status) => {
      console.log(status);
    }
    ).catch(error=>{
      console.error(error)
    })
  }

  pickMultiple() {
    choose = true;
    ImagePicker.openPicker({
      multiple: true,
      maxFiles: 6,
      compressImageMaxWidth: 300,
      compressImageQuality: 0.6,
    }).then(images => {
      this.setState({
        images: images
      });
      // console.log(images)
    }).catch(e => alert(e));
  }

  render() {
    // var images = post.images;
    // const image = require(this.props.image.path);
    return (
      <View style={style.cardDisplay}>
        
        <TextInput
          multiline = {true}
          numberOfLines = {8}
          onChangeText = {(text) => this.handleChange(text)}
          style={{margin: 10}}
          placeholder = "Typing Here ..."
        />
        <Divider style={style.devider} />
        
        <View style={{flex:1, flexDirection: 'row', flexWrap: 'wrap', }}>
          {this.state.images.map((item, index) => {
           return (
             <TouchableOpacity 
                activeOpacity={ 0.75 }
                style={style.postImg} 
                onPress = {() => this.deliteImage(item)}
                key = {index}
                >
                <Image style={style.postImg} source={{uri: item.path}} />
              </TouchableOpacity>
           );
         })
         } 
         <TouchableOpacity 
            activeOpacity={ 0.75 }
            style={{marginBottom: 200} }
            onPress = {() => this.pickMultiple()}
            key = {7}
            >
            <Image style={style.postImg} source={require('../static/img/plus.png')} />
          </TouchableOpacity>
           
        </View>

        
        <Divider style={{marginTop: 100}} />
        <Button 
          title="Send"
          color={sysColor.white}
          onPress = {() => {this.handleSubmit()}}
          buttonStyle={{backgroundColor: sysColor.green, marginTop: 20}}
          borderRadius= {5}
          fontWeight={"bold"}
        />
           
      </View>
    )
  }

}

