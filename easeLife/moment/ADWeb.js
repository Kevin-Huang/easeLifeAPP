import React from 'react';
import {  WebView } from 'react-native';


import sysColor from '../config/SysColor';
import style from '../static/style';

export default class ADWeb extends React.Component {
  constructor() {
    super();
  }

  render() {
    console.log(this.props.navigation.state.params)
    return (
      <WebView
        source={{uri: this.props.navigation.state.params.link}}
        style={{}}
      />
    )
  }
}