import dishsUrl from '../config/DishsUrl';

var dishApi = {
    getDishs(shopId){
        var url = dishsUrl.dish + shopId;
        console.log('loading data');
        return fetch(url).then((res) => res.json());
    }
};

module.exports = dishApi;