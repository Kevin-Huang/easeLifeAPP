
import sysUri from '../config/SysUri';


const shopApi = {
    getShops(){
        var uri = sysUri.shop;

        console.log('loading data');
        return fetch(uri).then((res) => res.json());
    }
};

module.exports = shopApi;