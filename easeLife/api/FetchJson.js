 class FetchJson {
    static query(uri){
        return fetch(uri, {
            method: 'GET',
            headers:{
                Accept: 'application/json',
            }
        }).then((res) => {
            console.log(res);
            if (res.headers.get("content-length") == 0){
                console.log("fetch result is empty");
                return JSON.parse('{}');
            }
            return res.json()
        }).catch((error)=>{
            console.log("Error when parse response in FetchJson");
            console.log(error);
            // return JSON.parse('{"statusText":"ERROR"}');
        });
    }

}
export default FetchJson;