import React from 'react';
import { StyleSheet,  Text, View, ScrollView, FlatList, Image, AsyncStorage, Dimensions, TouchableOpacity} from 'react-native';
import EaseLifeBase from './EaseLifeBase';

import style from '../static/style';
import sysColor from '../config/SysColor';
import sysUri from '../config/SysUri';

export default class ProtoPostOperate extends EaseLifeBase{
    constructor() {
        super();
    }

    loadAD(){
        fetch(sysUri.AD).then((res) => {
          return res.json();
        }).then((res)=>{
          var tempMap = this.state.ad
          res.map((item, index) =>{
            tempMap.set(item.IDAD, item)
          })
          this.setState({
            ad: tempMap,
          })
        }).catch(error=>{
          console.error(error)
        })
      }

      loadFavorite(IDUSER){
        fetch(sysUri.myFavorite+'/'+IDUSER).then((res) => {
          if(res.headers.get("content-length") > 2){
            var tempMap = this.state.myFavorite
            res.json().then((json) => {
            json.map((item, index) =>{
              tempMap.set(item.IDPOST,item)
            })
            this.setState({
              myFavorite: tempMap
            })
           }).catch(error=>{console.error(error)})}
        }).catch(error=>{
          console.error(error)
        })
      }

    

    changeEle(key, value){
        switch(key){
          case 'DELETEPOST': 
              console.log("delete post")
              posts = this.state.posts
              posts.delete(value)
              this.setState({
                posts: posts
              })
              break
          case 'ADDFAV':
              var favorite = this.state.myFavorite
              favorite.set(value.IDPOST, value)
              this.setState({
                myFavorite: favorite
              })
              break
          case 'DELFAV':
              var favorite = this.state.myFavorite
              favorite.delete(value.IDPOST)
              this.setState({
                myFavorite: favorite
              })
              break
          case 'RENEWPOST':
              Alert.alert(
              'Renew Post',
              'Renew post will cost 1 point. Are you sure to renew this post?',
              [
                {text: 'Renew', onPress: ()=> {
                  
                }},
                {text: 'Cancel', onPress: ()=> {console.log("Cancle")}}
              ],
              {cancelable: true}
            );
              break
    
          default: break;
        }
      }

}