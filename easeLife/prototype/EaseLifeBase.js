import React from 'react';
import {  Text, View, ScrollView, FlatList, Image, AsyncStorage, ActivityIndicator, Dimensions} from 'react-native';

import style from '../static/style';
import sysColor from '../config/SysColor';


export default class EaseLifeBase extends React.Component{
    constructor() {
        super();
    }

    async signonCheck(){
        console.log("Going in signonCheck ...")
           await AsyncStorage.getItem('Profile').then((profile)=> {
            if (profile !== null){
                this.setState({
                  isChecking: false,
                  profile: JSON.parse(profile),
                })
            }
          }).catch((error) => {
              this.setState({
                isChecking: false,
                error:true,
                errorInfo:"Local profile loading error ...",
              })
              console.error(error);
            });
      }
    renderLoadingView(){
        return (
            <View style={style.loading}>
                <ActivityIndicator
                    animating={true}
                    style={{height: 80}}
                    color={sysColor.green}
                    size="large"
                />
            </View>
        )
        }
    renderErrorView(error) {
        return (
            <View style={style.cardDisplay}>
                <Text>
                    {error}
                </Text>
            </View>
        );
    }
    renderNetworkInfo(error) {
      console.log(error)
      return (
          <View style={style.cardDisplay}>
              <Text>
                  Network: {error}
              </Text>
          </View>
      );
  }
}