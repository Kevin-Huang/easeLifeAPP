
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Image} from 'react-native';
import { Badge, Grid, Col, Row, Tile,Avatar, Divider, Card, List, ListItem } from 'react-native-elements';
import { StackNavigator } from 'react-navigation';

import sysColor from '../config/SysColor';
import style from '../static/style';
import sysUri from '../config/SysUri';

export default class DisplayShop extends React.Component{
    constructor() {
    super();
    // console.log("display shop");
   
  }

    render(){
        var shop = this.props.shop;
        var index = this.props.index;
        var idShop = this.props.shop.idSHOP;
        // console.log(shop);
        return (
        <View style={{backgroundColor:sysColor.white,}}>
            <Grid
            containerStyle={{ padding:5}}>
            <Col size={1} containerStyle={{padding:10}}>
                <Avatar
                    large
                    source={{uri: sysUri.loadImg+'/'+this.props.shop.logo}}
                    onPress={() => this.props.navigation.navigate('DishList', {IdShop: idShop, NameShop: shop.name})}
                    activeOpacity={0.7}
                />
            </Col>
            <Col size={3} containerStyle={{margin: 5}}>
                <Row><Text style={{fontWeight: 'bold'}}>{shop.name}</Text></Row>
                <Row><Text style={{fontSize: 10, textAlignVertical: 'center'}}>Address</Text></Row>
                <Row><Text>{shop.phone}</Text></Row>
                <Row></Row>
                <Row><Text style={{fontSize: 10}}>{shop.openTimeMorning}   {shop.openTimeAfternoon}</Text></Row>
            </Col>
            </Grid>
            <Divider style= { style.devider }/>
        </View>
        );
    }

}

