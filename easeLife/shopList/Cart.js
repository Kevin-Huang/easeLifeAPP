
import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import { Badge, Grid, Col, Row, Tile, Icon, Avatar, Divider, Card, List, ListdishId, Button } from 'react-native-elements';
import { StackNavigator, NavigationActions } from 'react-navigation';

import sysColor from '../config/SysColor';
import {dishInfo, cartInfo, shopInfo, dishCache} from './dishCart';
import SaveShopInfo from './SaveShopInfo';
import SaveCartInfo from './SaveCartInfo';
import ShopDish from './ShopDish';
import GetShopName from './GetShopName';
import style from '../static/style';
import sysUri from '../config/SysUri';

export default class Cart extends React.Component {
    constructor(){
        super();
    }

    static navigationOptions = ({navigation, screenProps}) => ({
        title: 'Cart',

        // headerLeft:
        // <Button
        //     title = {'asdas'}
        //     onPress = {() => navigation.navigate('Cart')}
        // />
        
    });


    render(){
        var shopView = [];
        for(let [shopId, dishArray] of cartInfo){
            shopView.push(
                <View key={shopId} style={{backgroundColor:sysColor.white, margin: 5, padding: 5, marginTop:10}}>
                    <Text sylte={{fontSize: 20}}>TTT{GetShopName.getShopName(shopId)}</Text>
                    <Divider style= { style.devider }/>
                    <ShopDish dishes={dishArray} />
                    <Divider style= { style.devider }/>
                </View>
                
            )
        }
        console.log('print shop view array');
        console.log(shopView);
       
        return(
            <ScrollView>
                {shopView} 
            </ScrollView>
        )
    }
}