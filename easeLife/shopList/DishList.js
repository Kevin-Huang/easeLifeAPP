
import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import { Badge, Grid, Col, Row, Tile, Icon, Avatar, Divider, Card, List, ListItem, Button } from 'react-native-elements';
import { StackNavigator, NavigationActions } from 'react-navigation';
import { observable } from 'mobx';

import sysColor from '../config/SysColor';
import dishApi from '../api/DishApi';
import dishsUrl from '../config/DishsUrl';
import {dishInfo, cartInfo, shopInfo, dishCache} from './dishCart';
import SaveShopInfo from './SaveShopInfo';
import SaveCartInfo from './SaveCartInfo';
import sysUri from '../config/SysUri';
import style from '../static/style';

class TotalCart {
    static getSumTotal(){
        var sumTotal = 0;
        for(let [item, number] of shopInfo){
            sumTotal = sumTotal + Number(number);
        }
        return sumTotal;
    }
}

export default class DishList extends React.Component {

    constructor() {
        super();
        this.state = {
            dishs: [],
            isCache: 'false',
        }
    }

    
    static navigationOptions = ({navigation, screenProps}) => ({
        title: navigation.state.params.NameShop,

        headerRight:
        <Button
            icon={{name: 'add-shopping-cart'}}
            title = {navigation.state.params.CheckOut}
            buttonStyle = {{backgroundColor: sysColor.pink, height:30, borderWidth:1, borderColor:sysColor.white}}
            onPress = {() => navigation.navigate('Cart')}
        />
    });

    componentWillMount(){
        const {params} = this.props.navigation.state;
        const shopId = params.IdShop;
        const name = params.NameShop;
        const setParamsAction = NavigationActions.setParams({
            params: { CheckOut: '$' + TotalCart.getSumTotal() },
            key: this.props.navigation.state.key,
        })
        this.props.navigation.dispatch(setParamsAction)
        dishApi.getDishs(shopId).then((res) => {
            console.log(res);
            this.setState({
                dishs: res,
                isCache: 'true'
            })
        });
    }

    render() {
        console.log(this.state.dishs);
        console.log(this.props.navigation);
        return (
            <ScrollView>
                {this.state.dishs.map((item, index) => {
                    dishCache.set(item.IDSERVICE, item);
                    {/* console.log(dishCache); */}
                    return (
                        <View key={index} style={{ backgroundColor:sysColor.white}} >
                            <Grid >
                                <Col size={1} containerStyle={{margin:10}}>
                                    <Avatar
                                        large
                                        source={{uri: sysUri.loadImg+'/'+item.PHOTO}}
                                        activeOpacity={0.7}
                                    />
                                </Col>
                                <Col size={3} containerStyle={{padding: 10}}>
                                    <Row><Text style={{fontWeight: 'bold'}}>{item.NAME}</Text></Row>
                                    <Row><Text style={{}}>{item.LABELS}</Text></Row>
                                    <Row></Row>
                                    <Row><Text style={{}}>{item.PRICE}</Text></Row>
                                </Col>
                                <Col size={1} containerStyle={{ padding: 10}}>
                                    
                                    <Row><Icon
                                        name = "add-shopping-cart"
                                        onPress={() => 
                                            {
                                                if(dishInfo.has(item.IDSERVICE)){
                                                    dishInfo.set(item.IDSERVICE, dishInfo.get(item.IDSERVICE) + 1);
                                                }else{
                                                    dishInfo.set(item.IDSERVICE, 1);
                                                }
                                                SaveShopInfo.saveShopInfo(item, 2);
                                                SaveCartInfo.saveCartInfo(item.IDSHOP, item.IDSERVICE);
                                                const setParamsAction = NavigationActions.setParams({
                                                    params: { CheckOut: '$' + TotalCart.getSumTotal() },
                                                    key: this.props.navigation.state.key,
                                                })
                                                this.props.navigation.dispatch(setParamsAction)
                                            }
                                        }
                                    /></Row>
                        
                                </Col>
                            </Grid>
                             <Divider style= { style.devider }/> 
                        </View>        
                    )
                })}  
            </ScrollView>
        );
    }

    componentDidCount(){
        if(this.state.isCache == 'false'){
            console.log('dishes load again load again');
        }
        const setParamsAction = NavigationActions.setParams({
            params: { CheckOut: '$' + TotalCart.getSumTotal() },
            key: this.props.navigation.state.key,
        })
        this.props.navigation.dispatch(setParamsAction)
    }

}