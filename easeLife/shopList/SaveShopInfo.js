import {dishInfo, cartInfo, shopInfo, dishCache} from './dishCart';

export default class SaveShopInfo {
    static saveShopInfo(dishItem, i) {
        temp = Math.pow(-1, i)
        if(shopInfo.has(dishItem.IDSHOP)){
            shopInfo.set(dishItem.IDSHOP, shopInfo.get(dishItem.IDSHOP) +  temp * Number(dishItem.PRICE));
        }else{
            shopInfo.set(dishItem.IDSHOP, Number(dishItem.PRICE));
        }
        console.log(shopInfo);
    }
}