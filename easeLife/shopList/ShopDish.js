
import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import { Badge, Grid, Col, Row, Tile, Icon, Avatar, Divider, Card, List, ListdishId, Button } from 'react-native-elements';
import { StackNavigator, NavigationActions } from 'react-navigation';

import sysColor from '../config/SysColor';
import {dishInfo, cartInfo, shopInfo, dishCache} from './dishCart';
import SaveShopInfo from './SaveShopInfo';
import SaveCartInfo from './SaveCartInfo';
import style from '../static/style';
import sysUri from '../config/SysUri';

export default class ShopDish extends React.Component {
    constructor(){
        super();
        this.state = {
            dishNumber: dishInfo,
        }
    }

    render(){
        var shopDish = this.props.dishes
        var shopView = []
        for(let dishId of shopDish){
            var dishNo = dishInfo.get(dishId)
            console.log('push into cartView');
            shopView.push(
                <View key={dishId} style={{margin:5, backgroundColor: sysColor.white, marginLeft: 30}}>
                    <Grid >
                        <Col size={1} containerStyle={{margin:5}}>
                            <Avatar
                                large
                                source={{uri: sysUri.loadImg+'/'+dishCache.get(dishId).PHOTO}}
                                activeOpacity={0.7}
                            />
                        </Col>
                        <Col size={3} containerStyle={{padding: 5, margin: 5, marginLeft: 15}}>
                            <Row><Text style={{fontWeight: 'bold', fontSize: 14}}>{'Name: ' + dishCache.get(dishId).NAME}</Text></Row>
                            <Row><Text style={{}}>{'Price: ' + dishCache.get(dishId).PRICE}</Text></Row>
                            {/* <Row><Text style={{fontWeight: 'bold'}}>{'Number: ' + dishNo}</Text></Row> */}
                            <Row><Text style={{}}>{'Total Price: ' + Number(dishNo) * Number(dishCache.get(dishId).PRICE)}</Text></Row>
                        </Col>
                        <Col size={1} containerStyle={{ padding: 5, }}>
                            <Row >
                                <Col size={1}><Text 
                                    style = {{ fontSize: 18}}
                                    activeOpacity={0.7}
                                    onPress = {() => {
                                        var temp = this.state.dishNumber
                                        newValue = temp.get(dishId) + 1
                                        temp.set(dishId, newValue)

                                        this.setState({
                                            dishNumber: temp,
                                        })
                                        SaveShopInfo.saveShopInfo(dishCache.get(dishId), 2);
                                        
                                        {/* console.log(this.state.dishNumber) */}
                                    }} 
                                >{'+'}</Text></Col>
                                <Col size={1}><Text style={{ fontSize: 16}}>{this.state.dishNumber.get(dishId)}</Text></Col>
                                <Col size={1}><Text 
                                    style = {{fontSize: 18}}
                                    activeOpacity={0.7}
                                    onPress ={() => {
                                        if(this.state.dishNumber.get(dishId) > 0){
                                            var temp = this.state.dishNumber
                                            newValue = temp.get(dishId) - 1
                                            temp.set(dishId, newValue)
                                            this.setState({
                                                dishNumber: temp
                                            })
                                            SaveShopInfo.saveShopInfo(dishCache.get(dishId), 1);
                                        }else{
                                            var temp = this.state.dishNumber
                                            newValue = 0
                                            temp.set(dishId, newValue)
                                            this.setState({
                                                dishNumber: temp
                                            })
                                        }
                                        {/* console.log(this.state.dishNumber) */}
                                    }}
                                >{'-'}</Text></Col>
                            </Row>
                            
                        </Col>
                    </Grid>
                    <Divider style= { style.devider }/>
                </View>
            )
        }
        console.log('print shop view array');
        // console.log(shopView);
       
        return(
            <ScrollView>
                {shopView} 
            </ScrollView>
        )
    }
}