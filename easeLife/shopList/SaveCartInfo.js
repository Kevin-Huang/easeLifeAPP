import {dishInfo, cartInfo, shopInfo, dishCache} from './dishCart';

export default class SaveCartInfo {
    static saveCartInfo(shopId, dishId) {
        if(cartInfo.has(shopId)){
            if(!cartInfo.get(shopId).includes(dishId)){
                var t = cartInfo.get(shopId);
                t.push(dishId);
                cartInfo.set(shopId, t);
            }
        }else{
            var t = new Array();
            t.push(dishId);
            cartInfo.set(shopId, t);
        }
        console.log(cartInfo);
    }
}