const dishInfo = new Map();
const cartInfo = new Map();
const shopInfo = new Map();
const dishCache = new Map();

export {dishInfo, cartInfo, shopInfo, dishCache};