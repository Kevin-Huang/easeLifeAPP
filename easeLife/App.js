import React from 'react';
import { AppRegistry, StyleSheet, Text, View,AsyncStorage } from 'react-native';
import { Button, Tabs, Tab, Icon } from 'react-native-elements';
import {
  TabNavigator,
  StackNavigator
} from 'react-navigation';


import sysColor from './config/SysColor';
import Moment from './tabbar/Moment';
import Chat from './tabbar/Chat';
import ShopList from './tabbar/ShopList';
import Profile from './tabbar/Profile';
import NewPostView from './moment/NewPostView';
import ImageSwiper from './moment/ImageSwiper';
import ChangeIconView from './profile/ChangeIconView';
// import SignonSignup from './profile/SignonSignup';
import UserProfile from './profile/UserProfile';
import ChatWith from './chat/ChatWith';
import AddNewFriend from './chat/AddNewFriend';
import ADWeb from './moment/ADWeb';
import Search from './moment/Search';
import AddPoints from './profile/AddPoints';

import DishList from './shopList/DishList';
import Cart from './shopList/Cart';

import sysUri from './config/SysUri';
import FetchJson from './api/FetchJson';
import CacheUser from './cache/CacheUser';



const MainScreenNavigator = TabNavigator({
  Moment: { screen: Moment },
  Chat: { screen: Chat},
  ShopList: { screen: ShopList },
  Profile: { screen: Profile },

}, 
{
  tabBarOptions:{
    activeTintColor: sysColor.pink,
    inactiveTinkColor: sysColor.deepGray,
    
  },
  navigationOptions: {
    headerTintColor: sysColor.white,
    headerStyle: {
      backgroundColor: sysColor.pink,
    },
  },
});


const SubScreenNavigator = StackNavigator({
  Home:         { screen: MainScreenNavigator },
  NewPostView:  { screen: NewPostView,},
  Profile:      { screen: Profile },

  UserProfile:  { screen: UserProfile, 
      navigationOptions: ({navigation}) => ({
        title: `${navigation.state.params.name}`,})
      },
  ChatWith:     { screen: ChatWith,
      navigationOptions: ({navigation}) => ({
        title: `${navigation.state.params.friendName}` }) 
      },
  AddNewFriend: {screen: AddNewFriend,},
  ImageSwiper:  { screen: ImageSwiper},
  ChangeIconView: { screen: ChangeIconView,},
  DishList:     { screen: DishList,},
  Cart:         { screen: Cart },
  ADWeb:        { screen: ADWeb},
  Search:       { screen: Search},
  AddPoints:    { screen: AddPoints},

},
{
  navigationOptions: ({navigation}) => ({
    headerTintColor: sysColor.white,
    
    headerStyle: {
      backgroundColor: sysColor.pink,
    }
  })
}
);



// MainScreenNavigator.navigationOptions = {
//   title: 'Moment',
// };

export default class App extends React.Component {
  constructor() {
    super();
  }

  componentDidMount(){

    console.log("String App... ")
   

  }

  render() {
    return(
      <SubScreenNavigator />
    );
  }
}
